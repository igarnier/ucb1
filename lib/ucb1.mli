type awaiting_reward
type ready_to_move

module type Arm_sig =
sig
  type t
  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end

module Make : functor (Arm : Arm_sig) ->
sig
  type 'state t

  val create : Arm.t array -> ready_to_move t
  val next_action : ready_to_move t -> Arm.t * awaiting_reward t
  val set_reward : awaiting_reward t -> float -> ready_to_move t
  val total_rewards : ready_to_move t -> float
  val pp_stats : Format.formatter -> 'state t -> unit
end
