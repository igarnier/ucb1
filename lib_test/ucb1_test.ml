module Arm =
struct
  type t = int
  let compare = Pervasives.compare
  let pp = Format.pp_print_int
end

module Bandit = Ucb1.Make(Arm)

let arms_count = 150

let make_reward_dist () =
  let drift = Random.float 1.0 in
  let mean  = drift +. 0.5 in
  mean, (fun () -> drift +. Random.float 1.0)

let means, reward =
  let mean_and_rewards = List.init arms_count (fun _ -> make_reward_dist ()) in
  let means, rewards = List.split mean_and_rewards in
  let means = Array.of_list means in
  let rewards = Array.of_list rewards in
  means, (fun arm -> rewards.(arm) ())

let find_index_of_max (a : float array) =
  let index = ref 0 in
  let value = ref 0.0 in
  for i = 0 to Array.length a - 1 do
    if a.(i) > !value then
      (index := i ;
       value := a.(i))
  done ;
  (!index, !value)

let best, bestv = find_index_of_max means

let _ =
  Printf.printf "best arm: %d, mean = %f\n%!" best bestv

let chrono f =
  let start = Oclock.(gettime realtime) in
  let res   = f () in
  let stop  = Oclock.(gettime realtime) in
  Int64.(sub stop start), res

let time, bandit =
  chrono (fun () ->
      let bandit = Bandit.create (Array.init arms_count (fun i -> i)) in
      let rec loop bandit iter =
        if iter = 0 then
          bandit
        else
          (let act, bandit = Bandit.next_action bandit in
           let reward = reward act in
           let bandit = Bandit.set_reward bandit reward in
           loop bandit (iter - 1))
      in
      loop bandit 100000)

let () =
  Format.printf "stats:\n%a" Bandit.pp_stats bandit

let _ =
  let max = bestv *. 10_000.0 in
  let achieved = Bandit.total_rewards bandit in
  Printf.printf "Execution time: %Ld microseconds\n" (Int64.div time 1_000L) ;
  Printf.printf "regret: %f - %f = %f\n" max achieved (max -. achieved)
